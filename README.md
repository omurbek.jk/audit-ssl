Audit SSL/TLS
============================
This ruleset will perform SSL/TLS audit

## Description
This ruleset runs dev-sec ssl baseline audit

* ![DevSec SSL Baseline](https://github.com/dev-sec/ssl-baseline "Inspec profile github link")

## Hierarchy



## Required variables with no default

**None**


## Required variables with default

**None**


## Optional variables with default

### `AUDIT_SSL_PROFILES_ALERT_LIST`:
  * description: 
  * default: ssl-baseline


## Optional variables with no default

### `FILTERED_OBJECTS`:
  * description: JSON object of string or regex of aws objects to include or exclude and tag in audit

## Tags
1. Audit
1. Best Practices

## Categories


## Diagram


## Icon


