coreo_agent_audit_profile 'ssl-baseline' do
    action :define
    profile 'https://github.com/dev-sec/ssl-baseline/archive/master.zip'
    timeout 120
end

coreo_agent_rule_runner 'audit-ssl-profiles' do
    action :run
    profiles ${AUDIT_SSL_PROFILES_ALERT_LIST}
    filter(${FILTERED_OBJECTS}) if ${FILTERED_OBJECTS}
end